#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "eccezioni.h"
#include "stardelta.h"
#include <iostream>
#include <sstream>
using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->tabWidget->setTabText(0, "Colori");
    ui->tabWidget->setTabText(1, "Resistenze");
    ui->lineEdit_input->setPlaceholderText("(R,G,B)");
    ui->lineEdit_percentuale->setPlaceholderText("20");
    ui->radio_4->setChecked(true);
    ui->comboBox_banda5->setEnabled(false);
    ui->comboBox_banda6->setEnabled(false);
    ui->label_stardelta1->setVisible(false);
    ui->label_stardelta2->setVisible(false);
    ui->label_stardelta3->setVisible(false);
    ui->label_insalpha->setVisible(false);
    ui->label_insbeta->setVisible(false);
    ui->label_alpha_formula->setVisible(false);
    ui->label_beta_formula->setVisible(false);
    ui->btn_ohmtemp->setEnabled(false);
    ui->label_ohmtemp->setEnabled(false);
    ui->lineEdit_temp_attuale->setEnabled(false);
    ui->lineEdit_temp_finale->setEnabled(false);
    ui->lineEdit_temperatura->setPlaceholderText("Temperatura in Kelvin");
    ui->lineEdit_media_frequenze->setPlaceholderText("Media frequenze in Hz");
    ui->lineEdit_intensita->setPlaceholderText("Intensità corrente");
    ui->lineEdit_intervallo_tempo->setPlaceholderText("Intervallo in scondi");
    ui->lineEdit_temp_attuale->setPlaceholderText("Temperatura attuale");
    ui->lineEdit_temp_finale->setPlaceholderText("Temperatura finale");
    ui->lineEdit_diff_potenziale->setPlaceholderText("Differenza di potenziale");
    QPixmap pix(":/new/prefix1/stardelta.png");
    ui->label_image->setPixmap(pix);
    comboBoxFill();
    setSfondo(ui->widget);
}

MainWindow::~MainWindow()
{
    delete ui;
}

Colore MainWindow::ColoreDaGUI(const QString& str) const{
    QByteArray ba = str.toLatin1();
    const char *ba_to_ch = ba.data();
    streambuf* orig = cin.rdbuf();
    istringstream input1(ba_to_ch);
    cin.rdbuf(input1.rdbuf());
    Colore c;
    try{cin>>c;}
    catch (err_sint){throw err_sint();}
    catch (fine_file){throw fine_file();}
    catch (err_R){throw err_R();}
    catch (err_G){throw err_G();}
    catch (err_B){throw err_B();}
    cin.rdbuf(orig);
    return c;
}

void MainWindow::comboBoxFill(){
    QStringList elementi;
    ui->comboBox_banda1->clear();
    ui->comboBox_banda2->clear();
    ui->comboBox_banda3->clear();
    ui->comboBox_banda4->clear();
    ui->comboBox_banda5->clear();
    ui->comboBox_banda6->clear();
    int n_bande = getNumeroBande();
    switch(n_bande){
        case 4:
            elementi<<"Nero"<<"Marrone"<<"Rosso"<<"Arancione"<<"Giallo"<<"Verde"<<"Blu"<<"Viola"<<"Grigio"<<"Bianco";
            ui->comboBox_banda1->addItems(elementi);
            ui->comboBox_banda2->addItems(elementi);
            elementi.clear();
            elementi<<"Argento"<<"Oro"<<"Nero"<<"Marrone"<<"Rosso"<<"Arancione"<<"Giallo"<<"Verde"<<"Blu"<<"Viola";
            ui->comboBox_banda3->addItems(elementi);
            elementi.clear();
            elementi<<"Argento"<<"Oro";
            ui->comboBox_banda4->addItems(elementi);
            elementi.clear();
        break;
        case 5:
            elementi<<"Nero"<<"Marrone"<<"Rosso"<<"Arancione"<<"Giallo"<<"Verde"<<"Blu"<<"Viola"<<"Grigio"<<"Bianco";
            ui->comboBox_banda1->addItems(elementi);
            ui->comboBox_banda2->addItems(elementi);
            ui->comboBox_banda3->addItems(elementi);
            elementi.clear();
            elementi<<"Argento"<<"Oro"<<"Nero"<<"Marrone"<<"Rosso"<<"Arancione"<<"Giallo"<<"Verde"<<"Blu"<<"Viola";
            ui->comboBox_banda4->addItems(elementi);
            elementi.clear();
            elementi<<"Argento"<<"Oro"<<"Marrone"<<"Rosso"<<"Verde"<<"Blu"<<"Viola"<<"Grigio";
            ui->comboBox_banda5->addItems(elementi);
            elementi.clear();
        break;
        case 6:
            elementi<<"Nero"<<"Marrone"<<"Rosso"<<"Arancione"<<"Giallo"<<"Verde"<<"Blu"<<"Viola"<<"Grigio"<<"Bianco";
            ui->comboBox_banda1->addItems(elementi);
            ui->comboBox_banda2->addItems(elementi);
            ui->comboBox_banda3->addItems(elementi);
            elementi.clear();
            elementi<<"Argento"<<"Oro"<<"Nero"<<"Marrone"<<"Rosso"<<"Arancione"<<"Giallo"<<"Verde"<<"Blu"<<"Viola";
            ui->comboBox_banda4->addItems(elementi);
            elementi.clear();
            elementi<<"Argento"<<"Oro"<<"Marrone"<<"Rosso"<<"Verde"<<"Blu"<<"Viola"<<"Grigio";
            ui->comboBox_banda5->addItems(elementi);
            elementi.clear();
            elementi<<"Nero"<<"Marrone"<<"Rosso"<<"Arancione"<<"Giallo"<<"Blu"<<"Viola";
            ui->comboBox_banda6->addItems(elementi);
            elementi.clear();
        break;
    }
}

void MainWindow::setSfondo(QWidget* w){
    QString str_col = ui->label_risultato->text();
    //nessuna eccezione perchè si assume che il colore del risultato sia sempre un colore valido
    Colore c = ColoreDaGUI(str_col);
    QPalette pal = palette();
    pal.setColor(QPalette::Background, QColor(c.getRosso(),c.getVerde(),c.getBlu()));
    w->setAutoFillBackground(true);
    w->setPalette(pal);
}

void MainWindow::setSfondoBande(int n_bande, int btn, bool initialize){
    QString str_col;
    switch(btn){
        case 1: str_col = ui->label_stardelta1->text(); break;
        case 2: str_col = ui->label_stardelta2->text(); break;
        case 3: str_col = ui->label_stardelta3->text(); break;
        case 4: str_col = ui->label_insalpha->text(); break;
        case 5: str_col = ui->label_insbeta->text(); break;
    }
    QString col[n_bande];
    Colore c;
    QWidget* PaletteBande[6];
    switch(btn){
        case 1:
            PaletteBande[0] = ui->r1_1;
            PaletteBande[1] = ui->r1_2;
            PaletteBande[2] = ui->r1_3;
            PaletteBande[3] = ui->r1_4;
            PaletteBande[4] = ui->r1_5;
            PaletteBande[5] = ui->r1_6;
        break;
        case 2:
            PaletteBande[0] = ui->r2_1;
            PaletteBande[1] = ui->r2_2;
            PaletteBande[2] = ui->r2_3;
            PaletteBande[3] = ui->r2_4;
            PaletteBande[4] = ui->r2_5;
            PaletteBande[5] = ui->r2_6;
        break;
        case 3:
            PaletteBande[0] = ui->r3_1;
            PaletteBande[1] = ui->r3_2;
            PaletteBande[2] = ui->r3_3;
            PaletteBande[3] = ui->r3_4;
            PaletteBande[4] = ui->r3_5;
            PaletteBande[5] = ui->r3_6;
        break;
        case 4:
            PaletteBande[0] = ui->r0_1;
            PaletteBande[1] = ui->r0_2;
            PaletteBande[2] = ui->r0_3;
            PaletteBande[3] = ui->r0_4;
            PaletteBande[4] = ui->r0_5;
            PaletteBande[5] = ui->r0_6;
        break;
        case 5:
            PaletteBande[0] = ui->r0_7;
            PaletteBande[1] = ui->r0_8;
            PaletteBande[2] = ui->r0_9;
            PaletteBande[3] = ui->r0_10;
            PaletteBande[4] = ui->r0_11;
            PaletteBande[5] = ui->r0_12;
        break;
    }
    for(int i=0; i<n_bande; i++){
        QPalette pal = palette();
        if(!initialize){
            col[i] = str_col.mid(str_col.indexOf("("), str_col.indexOf(")")+1);
            str_col.replace(str_col.indexOf("("), col[i].length(), "");
            c = ColoreDaGUI(col[i]);
            pal.setColor(QPalette::Background, QColor(c.getRosso(),c.getVerde(),c.getBlu()));
        }else{
            PaletteBande[i]->setVisible(true);
            pal.setColor(QPalette::Background, QColor(240,240,240));
        }
                switch(i){
                    case 0:PaletteBande[0]->setAutoFillBackground(true);PaletteBande[0]->setPalette(pal);break;
                    case 1:PaletteBande[1]->setAutoFillBackground(true);PaletteBande[1]->setPalette(pal);break;
                    case 2:PaletteBande[2]->setAutoFillBackground(true);PaletteBande[2]->setPalette(pal);break;
                    case 3:PaletteBande[3]->setAutoFillBackground(true);PaletteBande[3]->setPalette(pal);break;
                    case 4:PaletteBande[4]->setAutoFillBackground(true);PaletteBande[4]->setPalette(pal);break;
                    case 5:PaletteBande[5]->setAutoFillBackground(true);PaletteBande[5]->setPalette(pal);break;
                }
    }
}

void MainWindow::on_btn_reset_clicked(){
    clear_all_label();
    ostringstream stream;
    Colore aux;
    stream<<aux;
    ui->label_risultato->setText(QString::fromStdString(stream.str()));
    ui->label_eccezioni->clear();
}

void MainWindow::on_btn_colore_somma_clicked(){
    QString risultato = ui->label_risultato->text();
    QString addendo = ui->lineEdit_input->text();
    Colore c1 = ColoreDaGUI(risultato);
    ui->label_eccezioni->setText("");
    Colore c2;
    try{c2 = ColoreDaGUI(addendo);}
    catch (err_sint){ui->label_eccezioni->setText("errore sintattico");}
    catch (fine_file){ui->label_eccezioni->setText("errore EOF");}
    catch (err_R){ui->label_eccezioni->setText("errore sul valore relativo a Rosso");}
    catch (err_G){ui->label_eccezioni->setText("errore sul valore relativo a Verde");}
    catch (err_B){ui->label_eccezioni->setText("errore sul valore relativo a Blu");}
    ostringstream stream;
    stream << c1+c2;
    ui->label_risultato->setText(QString::fromStdString(stream.str()));
    setSfondo(ui->widget);
    clear_all_label();
}

void MainWindow::on_btn_colore_differenza_clicked(){
    QString risultato = ui->label_risultato->text();
    QString sottraendo = ui->lineEdit_input->text();
    Colore c1 = ColoreDaGUI(risultato);
    ui->label_eccezioni->setText("");
    Colore c2;
    try{c2 = ColoreDaGUI(sottraendo);}
    catch (err_sint){ui->label_eccezioni->setText("errore sintattico");}
    catch (fine_file){ui->label_eccezioni->setText("errore EOF");}
    catch (err_R){ui->label_eccezioni->setText("errore sul valore relativo a Rosso");}
    catch (err_G){ui->label_eccezioni->setText("errore sul valore relativo a Verde");}
    catch (err_B){ui->label_eccezioni->setText("errore sul valore relativo a Blu");}
    ostringstream stream;
    stream << c1-c2;
    ui->label_risultato->setText(QString::fromStdString(stream.str()));
    setSfondo(ui->widget);
    clear_all_label();
}

void MainWindow::clear_all_label(){
    ui->label_luminanza->clear();
    ui->label_esadecimale->clear();
    ui->label_confr->clear();

}

void MainWindow::on_btn_luminanza_clicked(){
    ostringstream stream;
    Colore aux;
    stream<<aux;
    ui->label_risultato->setText(QString::fromStdString(stream.str()));
    on_btn_colore_somma_clicked();
    QString risultato = ui->label_risultato->text();
    Colore c = ColoreDaGUI(risultato);
    ui->label_luminanza->setText(QString::fromStdString(to_string(c.luminanza())+" Nit"));

}

void MainWindow::on_btn_esadecimale_clicked(){
    ostringstream stream;
    Colore aux;
    stream<<aux;
    ui->label_risultato->setText(QString::fromStdString(stream.str()));
    on_btn_colore_somma_clicked();
    QString risultato = ui->label_risultato->text();
    Colore c = ColoreDaGUI(risultato);
    ui->label_esadecimale->setText(QString::fromStdString('#'+c.getHex()));

}

void MainWindow::on_btn_complementare_clicked(){
    ostringstream stream1;
    Colore aux;
    stream1<<aux;
    ui->label_risultato->setText(QString::fromStdString(stream1.str()));
    on_btn_colore_somma_clicked();
    QString risultato = ui->label_risultato->text();
    Colore c = ColoreDaGUI(risultato);
    c.coloreComplementare();
    ostringstream stream2;
    stream2 << c;
    ui->label_risultato->setText(QString::fromStdString(stream2.str()));
    setSfondo(ui->widget);
    clear_all_label();
}

void MainWindow::on_btn_mix_clicked(){
    QString risultato = ui->label_risultato->text();
    QString mix = ui->lineEdit_input->text();
    Colore c1 = ColoreDaGUI(risultato);
    ui->label_eccezioni->setText("");
    Colore c2;
    try{c2 = ColoreDaGUI(mix);}
    catch (err_sint){ui->label_eccezioni->setText("errore sintattico");}
    catch (fine_file){ui->label_eccezioni->setText("errore EOF");}
    catch (err_R){ui->label_eccezioni->setText("errore sul valore relativo a Rosso");}
    catch (err_G){ui->label_eccezioni->setText("errore sul valore relativo a Verde");}
    catch (err_B){ui->label_eccezioni->setText("errore sul valore relativo a Blu");}
    ostringstream stream;
    stream << c1.mixDueColori(c2);
    ui->label_risultato->setText(QString::fromStdString(stream.str()));
    setSfondo(ui->widget);
    clear_all_label();
}

void MainWindow::on_btn_scala_grigi_clicked(){
    ostringstream stream1;
    Colore aux;
    stream1<<aux;
    ui->label_risultato->setText(QString::fromStdString(stream1.str()));
    on_btn_colore_somma_clicked();
    QString risultato = ui->label_risultato->text();
    Colore c = ColoreDaGUI(risultato);
    c.convertiAScalaGrigi();
    ostringstream stream;
    stream << c;
    ui->label_risultato->setText(QString::fromStdString(stream.str()));
    setSfondo(ui->widget);
    clear_all_label();
}

void MainWindow::on_btn_percentuale_clicked(){
      ui->label_eccezioni->clear();
    if(ui->lineEdit_percentuale->text()!=""){
        ostringstream stream1;
        Colore aux;
        stream1<<aux;
        ui->label_risultato->setText(QString::fromStdString(stream1.str()));
        on_btn_colore_somma_clicked();
        QString risultato = ui->label_risultato->text();
        Colore c = ColoreDaGUI(risultato);
        QString str_perc = ui->lineEdit_percentuale->text();
        QByteArray ba = str_perc.toLatin1();
        const char *ba_to_ch = ba.data();
        streambuf* orig = cin.rdbuf();
        istringstream input1(ba_to_ch);
        cin.rdbuf(input1.rdbuf());
        int perc;
        cin>>perc;
        cin.rdbuf(orig);
        ostringstream stream;
        try{stream << c%perc;}
        catch(num_neg){ui->label_eccezioni->setText("Errore numero >0"); stream<<c%100;}
        catch(err_sint){ui->label_eccezioni->setText("Errore numero maggiore di 100"); stream<<c%100;}
        ui->label_risultato->setText(QString::fromStdString(stream.str()));
        setSfondo(ui->widget);
        clear_all_label();
    }
}

void MainWindow::on_btn_confronto_clicked(){
    QString risultato = ui->label_risultato->text();
    QString min = ui->lineEdit_input->text();
    Colore c1 = ColoreDaGUI(risultato);
    ui->label_eccezioni->setText("");
    Colore c2;
    try{c2 = ColoreDaGUI(min);}
    catch (err_sint){ui->label_eccezioni->setText("errore sintattico");}
    catch (fine_file){ui->label_eccezioni->setText("errore EOF");}
    catch (err_R){ui->label_eccezioni->setText("errore sul valore relativo a Rosso");}
    catch (err_G){ui->label_eccezioni->setText("errore sul valore relativo a Verde");}
    catch (err_B){ui->label_eccezioni->setText("errore sul valore relativo a Blu");}
    if(ui->label_eccezioni->text()!="")
        ui->label_confr->setText(QString::fromStdString("Inserire un colore"));
    else{
        if(c1.confronto(c2)=='<')
            ui->label_confr->setText(ui->label_risultato->text()+QString::fromStdString(" is less than ")+ui->lineEdit_input->text());
        if(c1.confronto(c2)=='>')
            ui->label_confr->setText(ui->label_risultato->text()+QString::fromStdString(" is bigger than ")+ui->lineEdit_input->text());
        if(c1.confronto(c2)=='=')
            ui->label_confr->setText(ui->label_risultato->text()+QString::fromStdString(" is equal to ")+ui->lineEdit_input->text());
    }
}

void MainWindow::on_btn_paraperta_clicked(){
ui->lineEdit_input->setText(QString::fromStdString("("));
}

void MainWindow::on_btn_parchiusa_clicked(){
ui->lineEdit_input->setText(ui->lineEdit_input->text()+QString::fromStdString(")"));
}

void MainWindow::on_btn_uno_clicked(){
ui->lineEdit_input->setText(ui->lineEdit_input->text()+QString::fromStdString("1"));
}

void MainWindow::on_btn_due_clicked(){
ui->lineEdit_input->setText(ui->lineEdit_input->text()+QString::fromStdString("2"));
}

void MainWindow::on_btn_tre_clicked(){
ui->lineEdit_input->setText(ui->lineEdit_input->text()+QString::fromStdString("3"));
}

void MainWindow::on_btn_quattro_clicked(){
ui->lineEdit_input->setText(ui->lineEdit_input->text()+QString::fromStdString("4"));
}

void MainWindow::on_btn_cinque_clicked(){
ui->lineEdit_input->setText(ui->lineEdit_input->text()+QString::fromStdString("5"));
}

void MainWindow::on_btn_sei_clicked(){
ui->lineEdit_input->setText(ui->lineEdit_input->text()+QString::fromStdString("6"));
}

void MainWindow::on_btn_sette_clicked(){
ui->lineEdit_input->setText(ui->lineEdit_input->text()+QString::fromStdString("7"));
}

void MainWindow::on_btn_otto_clicked(){
ui->lineEdit_input->setText(ui->lineEdit_input->text()+QString::fromStdString("8"));
}

void MainWindow::on_btn_nove_clicked(){
ui->lineEdit_input->setText(ui->lineEdit_input->text()+QString::fromStdString("9"));
}

void MainWindow::on_btn_zero_clicked(){
ui->lineEdit_input->setText(ui->lineEdit_input->text()+QString::fromStdString("0"));
}

void MainWindow::on_btn_virgola_clicked(){
ui->lineEdit_input->setText(ui->lineEdit_input->text()+QString::fromStdString(","));
}

resistenza* MainWindow::rDaGUI(const QString& str) const{
    string str_string = str.toStdString();
    resistenza* r1=NULL;
    switch(str_string.length()){
        case 4:
            r1 = new Res4bande(str_string[0], str_string[1], str_string[2], str_string[3]);
        break;
        case 5:
            r1 = new Res5bande(str_string[0], str_string[1], str_string[2], str_string[3], str_string[4]);
        break;
        case 6:
            r1 = new Res6bande(str_string[0], str_string[1], str_string[2], str_string[3], str_string[4], str_string[5]);
        break;
    }
    return r1;
}

void MainWindow::on_radio_4_clicked(){
    ui->radio_4->setChecked(true);
    ui->radio_5->setChecked(false);
    ui->radio_6->setChecked(false);
    ui->comboBox_banda5->setEnabled(false);
    ui->comboBox_banda6->setEnabled(false);
    ui->btn_ohmtemp->setEnabled(false);
    ui->label_ohmtemp->setEnabled(false);
    ui->lineEdit_temp_attuale->setEnabled(false);
    ui->lineEdit_temp_finale->setEnabled(false);
    comboBoxFill();
}

void MainWindow::on_radio_5_clicked(){
    ui->radio_4->setChecked(false);
    ui->radio_5->setChecked(true);
    ui->radio_6->setChecked(false);
    ui->comboBox_banda5->setEnabled(true);
    ui->comboBox_banda6->setEnabled(false);
    ui->btn_ohmtemp->setEnabled(false);
    ui->label_ohmtemp->setEnabled(false);
    ui->lineEdit_temp_attuale->setEnabled(false);
    ui->lineEdit_temp_finale->setEnabled(false);
    comboBoxFill();
}

void MainWindow::on_radio_6_clicked(){
    ui->radio_4->setChecked(false);
    ui->radio_5->setChecked(false);
    ui->radio_6->setChecked(true);
    ui->comboBox_banda5->setEnabled(true);
    ui->comboBox_banda6->setEnabled(true);
    ui->btn_ohmtemp->setEnabled(true);
    ui->label_ohmtemp->setEnabled(true);
    ui->lineEdit_temp_attuale->setEnabled(true);
    ui->lineEdit_temp_finale->setEnabled(true);
    comboBoxFill();
}

QString MainWindow::resistor_string_maker(int n_bande){
    QString inserimento; //= QString::number(n_bande)+"[";
    QComboBox* arrayBande[6];
    arrayBande[0]=ui->comboBox_banda1;
    arrayBande[1]=ui->comboBox_banda2;
    arrayBande[2]=ui->comboBox_banda3;
    arrayBande[3]=ui->comboBox_banda4;
    arrayBande[4]=ui->comboBox_banda5;
    arrayBande[5]=ui->comboBox_banda6;
    for(int i=0; i<n_bande; ++i){
        QString leggi = arrayBande[i]->currentText();
        if(leggi== "Nero") inserimento += "N";
        if(leggi== "Marrone") inserimento += "M";
        if(leggi== "Rosso") inserimento += "R";
        if(leggi== "Arancione") inserimento += "A";
        if(leggi== "Giallo") inserimento += "Y";
        if(leggi== "Verde") inserimento += "V";
        if(leggi== "Blu") inserimento += "B";
        if(leggi== "Viola") inserimento += "P";
        if(leggi== "Grigio") inserimento += "G";
        if(leggi== "Bianco") inserimento += "W";
        if(leggi== "Oro") inserimento += "o";
        if(leggi== "Argento") inserimento += "a";
    }
    return inserimento;
}

string MainWindow::ins_resistenza(int n_bande){
    QString inserimento = resistor_string_maker(n_bande);
    ostringstream stream;
    resistenza* r = rDaGUI(inserimento);
    if(inserimento.length()==4){
        stream << *(dynamic_cast<Res4bande*>(r));
    }else if(inserimento.length()==5){
        stream << *(dynamic_cast<Res5bande*>(r));
    }else{
        stream << *(dynamic_cast<Res6bande*>(r));
    }
    delete r;
    r = NULL;
    return stream.str();
}

int MainWindow::getNumeroBande(){
    int n_bande=0;
    if(ui->radio_4->isChecked()){
        n_bande=4;
    }
    if(ui->radio_5->isChecked()){
        n_bande=5;
    }
    if(ui->radio_6->isChecked()){
        n_bande=6;
    }
    return n_bande;
}

void MainWindow::on_btn_addR1_clicked(){
    int n_bande = getNumeroBande();
    switch(n_bande){
        case 4: ui->r1_5->setVisible(false);
                ui->r1_6->setVisible(false);
        break;
        case 5: ui->r1_5->setVisible(true);
                ui->r1_6->setVisible(false);
        break;
        case 6: ui->r1_5->setVisible(true);
                ui->r1_6->setVisible(true);
        break;
    }
    ui->label_stardelta1->setText(QString::fromStdString(ins_resistenza(n_bande)));
    setSfondoBande(n_bande, 1, false);
    ui->label_stardelta1->setText(resistor_string_maker(n_bande));
}

void MainWindow::on_btn_addR2_clicked(){
    int n_bande = getNumeroBande();
    switch(n_bande){
        case 4: ui->r2_5->setVisible(false);
                ui->r2_6->setVisible(false);
        break;
        case 5: ui->r2_5->setVisible(true);
                ui->r2_6->setVisible(false);
        break;
        case 6: ui->r2_5->setVisible(true);
                ui->r2_6->setVisible(true);
        break;
    }
    ui->label_stardelta2->setText(QString::fromStdString(ins_resistenza(n_bande)));
    setSfondoBande(n_bande, 2, false);
    ui->label_stardelta2->setText(resistor_string_maker(n_bande));
}

void MainWindow::on_btn_addR3_clicked(){
    int n_bande = getNumeroBande();
    switch(n_bande){
        case 4: ui->r3_5->setVisible(false);
                ui->r3_6->setVisible(false);
        break;
        case 5: ui->r3_5->setVisible(true);
                ui->r3_6->setVisible(false);
        break;
        case 6: ui->r3_5->setVisible(true);
                ui->r3_6->setVisible(true);
        break;
    }
    ui->label_stardelta3->setText(QString::fromStdString(ins_resistenza(n_bande)));
    setSfondoBande(n_bande, 3, false);
    ui->label_stardelta3->setText(resistor_string_maker(n_bande));
}

void MainWindow::on_btn_getValore_clicked(){
    int n_bande = getNumeroBande();
    ostringstream stream;
    QString inserimento = resistor_string_maker(n_bande);
    resistenza* r_getvalore = rDaGUI(inserimento);
    stream << r_getvalore->getValore();
    delete r_getvalore;
    r_getvalore = NULL;
    ui->label_getValore->setText(QString::fromStdString(stream.str()+" Ω"));
}

void MainWindow::on_btn_getRumore_clicked(){
    if(ui->lineEdit_temperatura->text()!="" && ui->lineEdit_media_frequenze->text()!=""){
        int n_bande = getNumeroBande();
        ostringstream stream;
        QString inserimento = resistor_string_maker(n_bande);
        resistenza* r_getrumore = rDaGUI(inserimento);
        try{stream << r_getrumore->getRumore(ui->lineEdit_temperatura->text().toDouble(), ui->lineEdit_media_frequenze->text().toDouble());}
        catch (num_neg){stream<<"Err:valore";}
        delete r_getrumore;
        r_getrumore = NULL;
        ui->label_getRumore->setText(QString::fromStdString(stream.str()));
    }
}

void MainWindow::on_btn_getJoule_clicked(){
    if(ui->lineEdit_intensita->text()!="" && ui->lineEdit_intervallo_tempo->text()!=""){
        int n_bande = getNumeroBande();
        ostringstream stream;
        QString inserimento = resistor_string_maker(n_bande);
        resistenza* r_getjoule = rDaGUI(inserimento);
        try{stream << r_getjoule->getJoule(ui->lineEdit_intensita->text().toDouble(),ui->lineEdit_intervallo_tempo->text().toDouble());}
        catch (num_neg){stream<<"Err:valore";}
        delete r_getjoule;
        r_getjoule = NULL;
        ui->label_getJoule->setText(QString::fromStdString(stream.str()));
    }
}

void MainWindow::on_btn_star_to_delta_clicked(){
    if(ui->label_stardelta1->text()!="TextLabel" && ui->label_stardelta2->text()!="TextLabel" && ui->label_stardelta3->text()!="TextLabel"){
        QPixmap pix(":/new/prefix1/stardelta.png");
        ui->label_image->setPixmap(pix);
        ostringstream stream;
        StarDelta sd;
        resistenza* r1=NULL, *r2=NULL, *r3=NULL;
        r1=rDaGUI(ui->label_stardelta1->text()); r2=rDaGUI(ui->label_stardelta2->text()); r3=rDaGUI(ui->label_stardelta3->text());
        sd.InsertDelta1(r1); sd.InsertDelta2(r2); sd.InsertDelta3(r3);
        try{
            sd.starToDelta();
            for(int i=0; i<3; ++i){
                stream<<sd.getDelta(i);
                switch(i){
                    case 0: ui->label_RA->setText(QString::fromStdString("Ra = "+stream.str()+" Ω")); break;
                    case 1: ui->label_RB->setText(QString::fromStdString("Rb = "+stream.str()+" Ω")); break;
                    case 2: ui->label_RC->setText(QString::fromStdString("Rc = "+stream.str()+" Ω"));break;
                }
                stream.str("");
            }
        }catch(err_divisione){ui->label_RA->setText("Err divisione"); ui->label_RB->setText("Err divisione"); ui->label_RC->setText("Err divisione");}
        for(int i=0; i<3; ++i){setSfondoBande(6, i+1, true);}
        delete r1; delete r2; delete r3;
        r1=NULL; r2=NULL; r3=NULL;
    }
}

void MainWindow::on_btn_delta_to_star_clicked(){
    if(ui->label_stardelta1->text()!="TextLabel" && ui->label_stardelta2->text()!="TextLabel" && ui->label_stardelta3->text()!="TextLabel"){
        QPixmap pix(":/new/prefix1/deltastar.png");
        ui->label_image->setPixmap(pix);
        ostringstream stream;
        StarDelta sd;
        resistenza* r1=NULL, *r2=NULL, *r3=NULL;
        r1=rDaGUI(ui->label_stardelta1->text()); r2=rDaGUI(ui->label_stardelta2->text()); r3=rDaGUI(ui->label_stardelta3->text());
        sd.InsertDelta1(r1); sd.InsertDelta2(r2); sd.InsertDelta3(r3);
        try{
            sd.deltaToStar();
            for(int i=0; i<3; ++i){
                stream<<sd.getDelta(i);
                switch(i){
                    case 0: ui->label_RA->setText(QString::fromStdString("Ra = "+stream.str()+" Ω")); break;
                    case 1: ui->label_RB->setText(QString::fromStdString("Rb = "+stream.str()+" Ω")); break;
                    case 2: ui->label_RC->setText(QString::fromStdString("Rc = "+stream.str()+" Ω"));break;
                }
                stream.str("");
            }
        }catch(err_divisione){ui->label_RA->setText("Err divisione"); ui->label_RB->setText("Err divisione"); ui->label_RC->setText("Err divisione");}
        for(int i=0; i<3; ++i){setSfondoBande(6, i+1, true);}
        delete r1; delete r2; delete r3;
        r1=NULL; r2=NULL; r3=NULL;
    }
}

void MainWindow::on_btn_rAlpha_clicked(){
    int n_bande = getNumeroBande();
    switch(n_bande){
        case 4: ui->r0_5->setVisible(false);
                ui->r0_6->setVisible(false);
        break;
        case 5: ui->r0_5->setVisible(true);
                ui->r0_6->setVisible(false);
        break;
        case 6: ui->r0_5->setVisible(true);
                ui->r0_6->setVisible(true);
        break;
    }
    ui->label_insalpha->setText(QString::fromStdString(ins_resistenza(n_bande)));
    setSfondoBande(n_bande, 4, false);
    ui->label_alpha_formula->setText(resistor_string_maker(n_bande));
}

void MainWindow::on_btn_rBeta_clicked(){
    int n_bande = getNumeroBande();
    switch(n_bande){
        case 4: ui->r0_11->setVisible(false);
                ui->r0_12->setVisible(false);
        break;
        case 5: ui->r0_11->setVisible(true);
                ui->r0_12->setVisible(false);
        break;
        case 6: ui->r0_11->setVisible(true);
                ui->r0_12->setVisible(true);
        break;
    }
    ui->label_insbeta->setText(QString::fromStdString(ins_resistenza(n_bande)));
    setSfondoBande(n_bande, 5, false);
    ui->label_beta_formula->setText(resistor_string_maker(n_bande));
}

void MainWindow::on_btn_minore_clicked(){
    if(ui->label_alpha_formula->text()!="TextLabel" && ui->label_beta_formula->text()!="TextLabel"){
        ostringstream stream;
        resistenza* r1 = rDaGUI(ui->label_alpha_formula->text());
        resistenza* r2 = rDaGUI(ui->label_beta_formula->text());
        if(*r1<*r2)stream<<"true";
        else stream<<"false";
        delete r1;
        delete r2;
        r1=NULL;
        r2=NULL;
        ui->label_true_false->setText(QString::fromStdString(stream.str()));
    }
}

void MainWindow::on_btn_maggione_clicked(){
    if(ui->label_alpha_formula->text()!="TextLabel" && ui->label_beta_formula->text()!="TextLabel"){
        ostringstream stream;
        resistenza* r1 = rDaGUI(ui->label_alpha_formula->text());
        resistenza* r2 = rDaGUI(ui->label_beta_formula->text());
        if(*r1>*r2)stream<<"true";
        else stream<<"false";
        delete r1;
        delete r2;
        r1=NULL;
        r2=NULL;
        ui->label_true_false->setText(QString::fromStdString(stream.str()));
    }
}

void MainWindow::on_btn_uguale_clicked(){
    if(ui->label_alpha_formula->text()!="TextLabel" && ui->label_beta_formula->text()!="TextLabel"){
        ostringstream stream;
        resistenza* r1 = rDaGUI(ui->label_alpha_formula->text());
        resistenza* r2 = rDaGUI(ui->label_beta_formula->text());
        if(*r1==*r2)stream<<"true";
        else stream<<"false";
        delete r1;
        delete r2;
        r1=NULL;
        r2=NULL;
        ui->label_true_false->setText(QString::fromStdString(stream.str()));
    }
}

void MainWindow::on_btn_serie_clicked(){
    if(ui->label_alpha_formula->text()!="TextLabel" && ui->label_beta_formula->text()!="TextLabel"){
        ostringstream stream;
        resistenza* r1 = rDaGUI(ui->label_alpha_formula->text());
        resistenza* r2 = rDaGUI(ui->label_beta_formula->text());
        try{stream<<resistenza::serie(*r1, *r2)<<" Ω";}
        catch (err_divisione){stream<<"Err:divisione, una resistenza vale 0 ohm";}
        delete r1;
        delete r2;
        r1=NULL;
        r2=NULL;
        ui->label_true_false->setText(QString::fromStdString(stream.str()));
    }
}

void MainWindow::on_btn_parallelo_clicked(){
    if(ui->label_alpha_formula->text()!="TextLabel" && ui->label_beta_formula->text()!="TextLabel"){
        ostringstream stream;
        resistenza* r1 = rDaGUI(ui->label_alpha_formula->text());
        resistenza* r2 = rDaGUI(ui->label_beta_formula->text());
        try{stream<<resistenza::parallelo(*r1, *r2)<<" Ω";}
        catch (err_divisione){stream<<"Err:divisione, una resistenza vale 0 ohm";}
        delete r1;
        delete r2;
        r1=NULL;
        r2=NULL;
        ui->label_true_false->setText(QString::fromStdString(stream.str()));
    }
}

void MainWindow::on_btn_ohm_clicked(){
    if(ui->lineEdit_diff_potenziale->text()!=""){
        int n_bande = getNumeroBande();
        ostringstream stream;
        QString inserimento = resistor_string_maker(n_bande);
        resistenza* r_leggeohm = rDaGUI(inserimento);
        try{stream << r_leggeohm->leggeohm(ui->lineEdit_diff_potenziale->text().toDouble());}
        catch(num_neg){stream<<"Err:valore";}
        catch(err_divisione){stream<<"Err:divisione";}
        delete r_leggeohm;
        r_leggeohm = NULL;
        ui->label_ohm->setText(QString::fromStdString(stream.str()));
    }
}

void MainWindow::on_btn_ohmtemp_clicked(){
    if(ui->lineEdit_temp_attuale->text()!="" && ui->lineEdit_temp_finale->text()!=""){
        ostringstream stream;
        QString inserimento = resistor_string_maker(6);
        Res6bande* r_ohmtemp = dynamic_cast<Res6bande*>(rDaGUI(inserimento));
        try{stream << r_ohmtemp->ohmTemp(ui->lineEdit_temp_attuale->text().toDouble(), ui->lineEdit_temp_finale->text().toDouble());}
        catch(num_neg){stream<<"ERR:T0>T";}
        delete r_ohmtemp;
        r_ohmtemp = NULL;
        ui->label_ohmtemp->setText(QString::fromStdString(stream.str())+" Ω");
    }
}
