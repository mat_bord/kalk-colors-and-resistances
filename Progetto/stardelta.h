#ifndef SARDELTA_H
#define SARDELTA_H
#include "resistenza.h"
#include "eccezioni.h"
class StarDelta
{
private:
    resistenza* starDeltaInput[3];
    double starDeltaOutput[3];
public:
    StarDelta();
    ~StarDelta();
    void deltaToStar();
    void starToDelta();
    void InsertDelta1( resistenza* &);
    void InsertDelta2( resistenza* &);
    void InsertDelta3( resistenza* &);
    double getDelta(int)const;
};

#endif // SARDELTA_H
