#ifndef RESISTENZA_H
#define RESISTENZA_H
#include <iostream>
#include <vector>
#include <map>
#include "colore.h"
#include <math.h>

using namespace std;

class Res4bande;
class Res5bande;
class Res6bande;

class resistenza
{
    friend istream& operator>>(istream&, Res6bande&);
    friend istream& operator>>(istream&, Res5bande&);
    friend istream& operator>>(istream&, Res4bande&);
private:
    static float constBoltzman;
    static map<char,Colore> colori_bande;
    static map<Colore,unsigned short int> valore;
    static map<Colore,float> tolleranza;
    static map<Colore,double> coloremoltiplicatore;

public:
    resistenza(){}
    virtual ~resistenza()=default;
    virtual double getValore() const=0;
    virtual void getallVal() const=0;
    bool operator<(const resistenza&)const;
    bool operator>(const resistenza&)const;
    bool operator==(const resistenza&)const;
    double leggeohm(double=NAN)const;
    static double serie(const resistenza&, const resistenza&);
    static double parallelo(const resistenza&, const resistenza&);
    double getRumore(double=NAN, double=NAN)const;
    double getJoule(double=NAN, double=NAN)const;
    virtual resistenza* clone()const=0;
    unsigned short int getValBanda(const Colore&)const;
    float getValToll(const Colore&)const;
    double getValMolt(const Colore&)const;
    Colore getCol(char c)const;

};

istream& operator>>(istream&, resistenza*&);

#endif // RESISTENZA_H
