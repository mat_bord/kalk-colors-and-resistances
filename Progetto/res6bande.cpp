#include "res6bande.h"
#include "listacolori.h"
#include "eccezioni.h"
#include <cmath>


map<Colore, short int> Res6bande::coeff_temperatura{ {C_NERO,200},{C_MARRONE,100},{C_ROSSO,50},{C_ARANCIONE,15},{C_GIALLO,25},{C_BLU,10},{C_VIOLA,5}};

Res6bande::Res6bande(char f1, char f2, char f3, char f4, char f5,char f6){
    v.clear();
    char c=f1;
    string::size_type pos;
    string valori_validi("NMRAYVBPGW");
    int cont=2;
    for(int i=0; i<4; i++){
            switch(i){
            case 0:
                if(cont==1) c=f2;
                if(cont==0) c=f3;
                pos = valori_validi.find(c);
                if(pos==string::npos && cont==2 ) throw err_banda1();
                if(pos==string::npos && cont==1) throw err_banda2();
                if(pos==string::npos && cont==0) throw err_banda3();
                if(cont>0){
                    i--;
                    cont--;
                }
                break;
            case 1:
                c=f4;
                valori_validi = "NMRAYVBPoa";
                pos = valori_validi.find(c);
                if(pos==string::npos) throw err_banda4();
                break;
            case 2:
                c=f5;
                valori_validi = "GPBVRMoa";
                pos = valori_validi.find(c);
                if(pos==string::npos) throw err_banda5();
                break;
            case 3:
                c=f6;
                valori_validi = "NMRAYBP";
                pos = valori_validi.find(c);
                if(pos==string::npos) throw err_banda6();
                break;
            }
            v.push_back(getCol(c));
     }
}


double Res6bande::getValore()const{
    return (getValBanda(v[0])*100+getValBanda(v[1])*10+getValBanda(v[2]))*getValMolt(v[3]);
}

void Res6bande::getallVal() const{
    cout<<getValore()<<" ohm +-"<<getValToll(v[4])<<"% "<<coeff_temperatura[v[5]]<<"ppm/k"<<endl;
}

Res6bande* Res6bande::clone() const{
    return new Res6bande(*this);
}


ostream& operator<<(ostream& os, const Res6bande& r){
    for(unsigned int i=0; i<r.v.size(); ++i) os<<r.v[i];
    return os;
}

istream& operator>>(istream& is, Res6bande& r){
    (r.v).clear();
    cout<<"   | N = nero | M = marrone | R = rosso | A = arancione | Y = giallo | V = verde | B = blu | P = viola | G = grigio |   "<<"   | W = bianco | o = oro | a = argento | esempio: [Y,P,A,o,a,N]"<<endl;
    char c;
    string::size_type pos;
    string valori_validi("NMRAYVBPGW");
    if(!(is>>c)) throw fine_file();
    if(c!='[') throw err_sint() ;
    //abbiamo letto la prima parentesi, dobbiamo leggere almeno 3 colori seguiti da ,
    int cont=2;
        for(int i=0; i<4; i++){
            if(!(is>>c)) throw fine_file();
            switch(i){
            case 0:
                pos = valori_validi.find(c);
                if(pos==string::npos && cont==2 ) throw err_banda1();
                if(pos==string::npos && cont==1) throw err_banda2();
                if(pos==string::npos && cont==0) throw err_banda3();
                if(cont>0){
                    i--;
                    cont--;
                }
                break;
            case 1:
                valori_validi = "NMRAYVBPoa";
                pos = valori_validi.find(c);
                if(pos==string::npos) throw err_banda4();
                break;
            case 2:
                valori_validi = "GPBVRMoa";
                pos = valori_validi.find(c);
                if(pos==string::npos) throw err_banda5();
                break;
            case 3:
                valori_validi = "NMRAYBP";
                pos = valori_validi.find(c);
                if(pos==string::npos) throw err_banda6();
                break;
            }
            (r.v).push_back(resistenza::colori_bande[c]);
            if(i<3){
               if(!(is>>c)) throw fine_file();
                if(c!=',') throw err_sint() ;
            }
        }
 if(!(is>>c)) throw fine_file();
 if(c!=']') throw err_sint();
 return is;
}

double Res6bande::ohmTemp(double T0, double T)const{
    cout<<"Inserisci la temperatura attuale"<<endl;
    if(isnan(T0))
        cin>>T0;
    cout<<"Inserisci la temmperatura finale"<<endl;
    if(isnan(T))
        cin>>T;
    if(T0>T) throw num_neg();
    return (getValore()*(1+coeff_temperatura[v[5]]*(T-T0)));
}
