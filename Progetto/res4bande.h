#ifndef RES4BANDE_H
#define RES4BANDE_H
#include "resistenza.h"

class Res4bande : public resistenza{
friend istream& operator>>(istream&, Res4bande&);
friend ostream& operator<<(ostream&, const Res4bande&);
private:
vector<Colore> v;
public:
    Res4bande(char='N', char='N', char='N', char='a');
    virtual double getValore() const;
    virtual void getallVal() const;
    virtual Res4bande* clone() const;
};

ostream& operator<<(ostream&, const Res4bande&);

istream& operator>>(istream&, Res4bande&);

#endif // RES4BANDE_H
