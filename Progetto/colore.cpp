#include "colore.h"
#include "eccezioni.h"
#include <iostream>
#include <cmath>
using namespace std;

Colore Colore::Colore_Bianco(255,255,255);

char Colore::hex[16]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

string Colore::getHex() const{
  string esa = "000000";//=new char [6];
  int col=Rosso ,i=0;
  while(i!=6){
    esa[i]=hex[col/16];
    i++;
    esa[i]=hex[col%16];
    i++;
    if(i==2)
        col=Verde;
    if(i==4)
        col=Blu;
  }
  return esa;
}

Colore Colore::operator+(const Colore& c)const{
    Colore somma;
    somma.Rosso = Rosso+c.Rosso;
    if(somma.Rosso > 255) somma.Rosso = 255;
    somma.Verde = Verde+c.Verde;
    if(somma.Verde > 255) somma.Verde = 255;
    somma.Blu = Blu+c.Blu;
    if(somma.Blu > 255) somma.Blu = 255;
    return somma;
}

Colore Colore::operator-(const Colore& c)const{
    Colore differenza;
    if(c.Rosso > Rosso)differenza.Rosso = 0;
    else differenza.Rosso = Rosso-c.Rosso;
    if(c.Verde > Verde)differenza.Verde = 0;
    else differenza.Verde = Verde-c.Verde;
    if(c.Blu > Blu)differenza.Blu = 0;
    else differenza.Blu = Blu-c.Blu;
    return differenza;
}

void Colore::coloreComplementare() {
    *this=Colore_Bianco-*this;
}

Colore Colore::mixDueColori(const Colore& c) const{
    Colore mix;
    mix.Rosso=(this->Rosso+c.Rosso)/2;
    mix.Verde=(this->Verde+c.Verde)/2;
    mix.Blu=(this->Blu+c.Blu)/2;
    return mix;
}

void Colore::convertiAScalaGrigi(){
    Blu=Verde=Rosso=(Rosso+Verde+Blu)/3;
}

unsigned short int Colore::getRosso()const{
    return Rosso;
}
unsigned short int Colore::getVerde()const{
    return Verde;
}
unsigned short int Colore::getBlu()const{
    return Blu;
}

Colore Colore::operator%(int n){
    if(n<0)
        throw num_neg();
    if(n>100)
        throw err_sint();
    Colore percentuale;
    percentuale.Rosso=round(static_cast<float>(Rosso)*n/100);
    percentuale.Verde=round(static_cast<float>(Verde)*n/100);
    percentuale.Blu=round(static_cast<float>(Blu)*n/100);
    return percentuale;
}

ostream& operator<<(ostream& os, const Colore& c){
    return os<<"("<<c.Rosso<<","<<c.Verde<<","<<c.Blu<<")";
}

istream& operator>>(istream& is, Colore& col){
    cout<<"Inserisci tre cifre da 0 a 255 seguendo la seguente sintassi ES: (0,124,255)"<<endl;
    //formato di input: (R,G,B)
    char c;
    unsigned short int R, G, B;
    string::size_type pos;
    string cifre("0123456789"); //serve a verificare che in input ci siano solo numeri
    if(!(is>>c)) throw fine_file();
    if(c!='(') throw err_sint();
    //abbiamo letto la prima parentesi
    //ROSSO
    if(!(is>>c)) throw fine_file(); //prima cifra di R
    pos = cifre.find(c);
    if(pos==string::npos) throw err_sint();
    R=pos;
    if(!(is>>c)) throw fine_file(); //possibile inserimento della seconda cifra di R
    while(c!=','){
        pos = cifre.find(c);
        if(pos==string::npos) throw err_sint();
        R = R*10+pos;
        if(!(is>>c)) throw fine_file(); //possibile inserimento della terza cifra di R
    }//uscita del ciclo == inserimento della virgola
    if(R >255) throw err_R();
    //col.Rosso = R;
    //VERDE
    if(!(is>>c)) throw fine_file(); //prima cifra di G
    pos = cifre.find(c);
    if(pos==string::npos) throw err_sint();
    G=pos;
    if(!(is>>c)) throw fine_file(); //possibile inserimento della seconda cifra di G
    while(c!=','){
        pos = cifre.find(c);
        if(pos==string::npos) throw err_sint();
        G = G*10+pos;
        if(!(is>>c)) throw fine_file(); //possibile inserimento della terza cifra di G
    }//uscita del ciclo == inserimento della virgola
    if(G >255) throw err_G();
    //col.Verde = G;
    //BLU
    if(!(is>>c)) throw fine_file(); //prima cifra di B
    pos = cifre.find(c);
    if(pos==string::npos) throw err_sint();
    B=pos;
    if(!(is>>c)) throw fine_file(); //possibile inserimento della seconda cifra di B
    while(c!=')'){
        pos = cifre.find(c);
        if(pos==string::npos) throw err_sint();
        B = B*10+pos;
        if(!(is>>c)) throw fine_file(); //possibile inserimento della terza cifra di B
    }//uscita del ciclo == inserimento della seconda parentesi
    if(B >255) throw err_B();
    col.Rosso = R;
    col.Verde = G;
    col.Blu = B;
    return is;
}

int Colore::luminanza()const{
    float l=0.2126*Rosso+0.7152*Verde+0.0722*Blu;
    return round(l);
}


char Colore::confronto(const Colore & c) const{
    if((Rosso+Verde+Blu)==(c.Rosso+c.Verde+c.Blu))return '=';
    if((Rosso+Verde+Blu)<(c.Rosso+c.Verde+c.Blu))return '<';
    else return '>';
}

bool Colore::operator<(const Colore & c) const{
    if((Rosso+Verde+Blu)<(c.Rosso+c.Verde+c.Blu))return true;
    else return false;
}

Colore& Colore::operator=(const Colore& c){
    if(this!=&c){
        Rosso=c.Rosso;
        Verde=c.Verde;
        Blu=c.Blu;
    }
    return *this;
}
