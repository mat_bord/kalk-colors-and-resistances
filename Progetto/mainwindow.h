#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "colore.h"
#include "res4bande.h"
#include "res5bande.h"
#include "res6bande.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btn_colore_somma_clicked();
    void on_btn_colore_differenza_clicked();
    void on_btn_luminanza_clicked();
    void on_btn_esadecimale_clicked();
    void on_btn_complementare_clicked();
    void on_btn_mix_clicked();
    void on_btn_scala_grigi_clicked();
    void on_btn_percentuale_clicked();
    void on_btn_confronto_clicked();
    void on_btn_paraperta_clicked();
    void on_btn_parchiusa_clicked();
    void on_btn_uno_clicked();
    void on_btn_due_clicked();
    void on_btn_tre_clicked();
    void on_btn_quattro_clicked();
    void on_btn_cinque_clicked();
    void on_btn_sei_clicked();
    void on_btn_sette_clicked();
    void on_btn_otto_clicked();
    void on_btn_nove_clicked();
    void on_btn_zero_clicked();
    void on_btn_virgola_clicked();
    void on_radio_4_clicked();
    void on_radio_5_clicked();
    void on_radio_6_clicked();
    void on_btn_addR1_clicked();
    void on_btn_addR2_clicked();
    void on_btn_addR3_clicked();
    void on_btn_getValore_clicked();
    void on_btn_getRumore_clicked();
    void on_btn_getJoule_clicked();
    void on_btn_star_to_delta_clicked();
    void on_btn_delta_to_star_clicked();
    void on_btn_rAlpha_clicked();
    void on_btn_rBeta_clicked();
    void on_btn_maggione_clicked();
    void on_btn_minore_clicked();
    void on_btn_uguale_clicked();
    void on_btn_serie_clicked();
    void on_btn_parallelo_clicked();
    void on_btn_ohm_clicked();
    void on_btn_ohmtemp_clicked();
    void on_btn_reset_clicked();

private:
    Ui::MainWindow *ui;
    Colore ColoreDaGUI(const QString&) const;
    resistenza* rDaGUI(const QString&) const;
    string ins_resistenza(int);
    QString resistor_string_maker(int);
    int getNumeroBande();
    void setSfondo(QWidget*);
    void setSfondoBande(int, int, bool);
    void clear_all_label();
    void comboBoxFill();
};

#endif // MAINWINDOW_H
