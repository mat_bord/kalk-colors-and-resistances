#include "mainwindow.h"
#include <QApplication>
#include <iostream>
#include "colore.h"
#include "resistenza.h"
#include "res4bande.h"
#include "res5bande.h"
#include "res6bande.h"
#include "eccezioni.h"
#include "listacolori.h"
#include "stardelta.h"
#include <vector>
using namespace std;
int main(int argc, char *argv[])
{

        if(argc==1){
            QApplication a(argc, argv);
            MainWindow w;
            w.show();
            return a.exec();
        }else if(argc==2){
                    Colore uno;
                    try{cin>>uno;}
                    catch (err_sint){cout<<"errore sintattico"<<endl; uno=Colore();}
                    catch (fine_file){cout<<"errore EOF"<<endl; uno=Colore();}
                    catch (err_R){cout<<"errore sul valore relativo a Rosso"<<endl; uno=Colore();}
                    catch (err_G){cout<<"errore sul valore relativo a Verde"<<endl; uno=Colore();}
                    catch (err_B){cout<<"errore sul valore relativo a Blu"<<endl; uno=Colore();}
                    cin.ignore(numeric_limits<streamsize>::max(), '\n');
                    Colore due;
                    try{cin>>due;}
                    catch (err_sint){cout<<"errore sintattico"<<endl; due=Colore();}
                    catch (fine_file){cout<<"errore EOF"<<endl; due=Colore();}
                    catch (err_R){cout<<"errore sul valore relativo a Rosso"<<endl; due=Colore();}
                    catch (err_G){cout<<"errore sul valore relativo a Verde"<<endl; due=Colore();}
                    catch (err_B){cout<<"errore sul valore relativo a Blu"<<endl; due=Colore();}
                    cin.ignore(numeric_limits<streamsize>::max(), '\n');
                    cout<<"Valore primo colore"<<endl;
                    cout<<uno<<endl;
                    cout<<"Valore secondo colore"<<endl;
                    cout<<due<<endl;
                    uno=uno-due;
                    cout<<"Primo meno secondo"<<endl;
                    cout<<uno<<endl;
                    Colore tre(0,0,0);
                    cout<<"Assegnazione del secondo ad un terzo"<<endl;
                    tre=due;
                    cout<<tre<<endl;
                    due=uno+due;
                    cout<<"Primo più secondo"<<endl;
                    cout<<due<<endl;
                    cout<<"Colore complementare"<<endl;
                    tre.coloreComplementare();
                    cout<<tre<<endl;
                    cout<<"Scala di grigi"<<endl;
                    tre.convertiAScalaGrigi();
                    cout<<tre<<endl;
                    cout<<"Mix del primo e del terzo"<<endl;
                    uno.mixDueColori(tre);
                    cout<<uno<<endl;
                    cout<<"n%"<<endl;
                    int n;
                    cout<<"Inserisci la percentuale maggiore di 0"<<endl;
                    cin>>n;
                    try{due%n;
                    cout<<due<<endl;}
                    catch(num_neg){cout<<"Errore numero <0"<<endl;}
                    catch(err_sint){cout<<"Errore numero magiore di 100"<<endl;}
                    cin.ignore(numeric_limits<streamsize>::max(), '\n');
                    cout<<"Luminanza";
                    cout<<due.luminanza()<<" Nit";

                    cout<<endl;
                  cout<<"RESISTENZE"<<endl;
                    resistenza* r1=nullptr;
                    try{cin>>r1;}
                    catch(err_nbande){cout<<"Errore numero bande"<<endl; r1=new Res4bande();}
                    cin.ignore(numeric_limits<streamsize>::max(), '\n');
                    cout<<"Valore prima resistenza r1"<<endl;
                    r1->getallVal();
                    resistenza* r2=nullptr;
                    try{cin>>r2;}
                    catch(err_nbande){cout<<"Errore numero bande"<<endl; r2=new Res4bande();}
                    cin.ignore(numeric_limits<streamsize>::max(), '\n');
                    cout<<"Valore seconda resistenza r2"<<endl;
                    r2->getallVal();
                    cout<<"r1<r2?"<<endl;
                    cout<<(r1<r2)<<endl;
                    cout<<"r1>r2?"<<endl;
                    cout<<(r1>r2)<<endl;
                    cout<<"r1==r2?"<<endl;
                    cout<<(r1==r2)<<endl;
                    cout<<"r1 e r2 in serie"<<endl;
                    cout<<resistenza::serie(*r1,*r2)<<" ohm"<<endl;
                    cout<<"r1 e r2 in parallelo"<<endl;
                    try{cout<<resistenza::parallelo(*r1,*r2)<<" ohm"<<endl;}
                    catch (err_divisione){cout<<"Err:divisione, una resistenza vale 0 ohm"<<endl;}
                    cout<<"Valore in ohm di r2"<<endl;
                    try{cout<<r2->leggeohm()<<"A"<<endl;}
                    catch(num_neg){cout<<"Err:valore"<<endl;}
                    catch(err_divisione){cout<<"Err:divisione"<<endl;}
                    cin.ignore(numeric_limits<streamsize>::max(), '\n');
                    resistenza* r3=nullptr;
                    try{cin>>r3;}
                    catch(err_nbande){cout<<"Errore numero bande"<<endl; r3=new Res4bande();}
                    cin.ignore(numeric_limits<streamsize>::max(), '\n');
                    cout<<"Valore terza resistenza r3"<<endl;
                    r3->getallVal();
                    cout<<"Rumore di r3"<<endl;
                    try{cout<<r3->getRumore()<<"volt"<<endl;}
                    catch (num_neg){cout<<"Err:valore"<<endl;}
                    cin.ignore(numeric_limits<streamsize>::max(), '\n');
                    cout<<"Joule di r1"<<endl;
                    try{cout<<r1->getJoule()<<"J"<<endl;}
                    catch(num_neg){cout<<"Err:valore"<<endl;}
                    cin.ignore(numeric_limits<streamsize>::max(), '\n');
                    StarDelta sd;
                    sd.InsertDelta1(r1);
                    sd.InsertDelta2(r2);
                    sd.InsertDelta3(r3);
                    cout<<"Delta->Star di r1, r2 e r3"<<endl;
                    try{sd.deltaToStar();
                    for(int i=0;i<3;i++)
                        cout<<sd.getDelta(i)<<"ohm"<<endl;}
                    catch(err_divisione){cout<<"Err: la somma delle resistenze vale 0"<<endl;}
                    sd.InsertDelta1(r1);
                    sd.InsertDelta2(r2);
                    sd.InsertDelta3(r3);
                    cout<<"Star->Delta di r1, r2 e r3"<<endl;
                    try{sd.starToDelta();
                    for(int i=0;i<3;i++)
                        cout<<sd.getDelta(i)<<"ohm"<<endl;}
                    catch(err_divisione){cout<<"Err: una o più resistenze valgono 0"<<endl;}
                    cout<<"Resistività al lavoro di r6"<<endl;
                    Res6bande* r6=dynamic_cast<Res6bande*>(r1);
                    if(r6){
                        try{cout<<r6->ohmTemp()<<" ohm"<<endl;}
                        catch(num_neg){cout<<"ERR:T0>T"<<endl;}
                        cin.ignore(numeric_limits<streamsize>::max(), '\n');
                    }
                    else{
                       cout<<"r1 non è a 6 bande"<<endl;
                    }
                    r6=dynamic_cast<Res6bande*>(r2);
                    if(r6){
                        try{cout<<r6->ohmTemp()<<" ohm"<<endl;}
                        catch(num_neg){cout<<"ERR:T0>T"<<endl;}
                        cin.ignore(numeric_limits<streamsize>::max(), '\n');
                    }
                    else{
                       cout<<"r2 non è a 6 bande"<<endl;
                    }
                    r6=dynamic_cast<Res6bande*>(r3);
                    if(r6){
                        try{cout<<r6->ohmTemp()<<" ohm"<<endl;}
                        catch(num_neg){cout<<"ERR:T0>T"<<endl;}
                        cin.ignore(numeric_limits<streamsize>::max(), '\n');
                    }
                    else{
                       cout<<"r3 non è a 6 bande"<<endl;
                    }
     }
}
