#ifndef ECCEZIONI
#define ECCEZIONI
    class err_sint{};   //errore sintattico
    class fine_file{};  //file finito prematuramente
    class err_R{};      //errore sul valore relativo a Rosso
    class err_G{};      //errore sul valore relativo a Verde
    class err_B{};      //errore sul valore relativo a Blu
    class err_banda1{}; //errore sul valore relativo alla prima banda
    class err_banda2{}; //errore sul valore relativo alla seconda banda
    class err_banda3{}; //errore sul valore relativo alla terza banda
    class err_banda4{}; //errore sul valore relativo alla quarta banda
    class err_banda5{}; //errore sul valore relativo alla quinta banda
    class err_banda6{}; //errore sul valore relativo alla sesta banda
    class err_divisione{};//errore divisione nel caso il denominatore sia zero
    class num_neg{};//errore sul numero negativo
    class err_nbande{}; //errore numero bande input
#endif // ECCEZIONI

