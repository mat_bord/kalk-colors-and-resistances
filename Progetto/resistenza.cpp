#include "resistenza.h"
#include "eccezioni.h"
#include <iostream>
#include <cstdlib>
#include "listacolori.h"
#include "res4bande.h"
#include "res5bande.h"
#include "res6bande.h"
#include <cmath>
using namespace std;

map<Colore,unsigned short int> resistenza::valore{ {C_NERO,0},{C_MARRONE,1},{C_ROSSO,2},{C_ARANCIONE,3},{C_GIALLO,4},{C_VERDE,5},{C_BLU,6},{C_VIOLA,7},{C_GRIGIO,8},{C_BIANCO,9}};

map<Colore, double> resistenza::coloremoltiplicatore{ {C_NERO,1},{C_MARRONE,10},{C_ROSSO,100},{C_ARANCIONE,1000},{C_GIALLO,10000},{C_VERDE,100000},{C_BLU,1000000},{C_VIOLA,10000000},{C_ORO,0.1},{C_ARGENTO,0.01} };

map<Colore, float> resistenza::tolleranza{{C_GRIGIO,0.05},{C_VIOLA,0.1},{C_BLU,0.25},{C_VERDE,0.5},{C_ROSSO,2},{C_MARRONE,1},{C_ORO,5},{C_ARGENTO,10}};

map<char, Colore> resistenza::colori_bande{ {'N',C_NERO},{'M',C_MARRONE},{'R',C_ROSSO},{'A',C_ARANCIONE},{'Y',C_GIALLO},{'V',C_VERDE},{'B',C_BLU},{'P',C_VIOLA},{'G',C_GRIGIO},{'W',C_BIANCO},{'o',C_ORO},{'a',C_ARGENTO} };

float resistenza::constBoltzman=1.38e-23;



bool resistenza::operator<(const resistenza& r)const{
    if(fabs(getValore()-r.getValore())<0.001)
        return false;
    else
    return getValore()<r.getValore();
}

bool resistenza::operator>(const resistenza& r)const{
    if(fabs(getValore()-r.getValore())<0.001)
        return false;
    else
    return getValore()>r.getValore();
}

bool resistenza::operator==(const resistenza& r)const{
    return fabs(getValore() - r.getValore()) < 0.001;
}

double resistenza::serie(const resistenza& r1, const resistenza& r2){
    return r1.getValore() + r2.getValore();
}

double resistenza::parallelo(const resistenza& r1, const resistenza& r2){
    if((r1.getValore()+r2.getValore())==0) throw err_divisione();
    return (r1.getValore()*r2.getValore())/(r1.getValore()+r2.getValore());
}

double resistenza::leggeohm(double v)const{
    cout<<"Inserisci la differenza di potenziale elettrico V"<<endl;
    if(isnan(v))
        cin>>v;
    if(v<0) throw num_neg();
    if(getValore())
        return v/getValore();
    else
        throw err_divisione();
}

double resistenza::getRumore(double k, double f)const{
    cout<<"Inserire la temperatura in kelvin"<<endl;
    if(isnan(k))
        cin>>k;
    if(k<0) throw num_neg();
    cout<<"Inserire la media delle frequenze considerate in Hz"<<endl;
    if(isnan(f))
        cin>>f;
    if(f<0) throw num_neg();
    return sqrt(4*constBoltzman*getValore()*k*f);
}

double resistenza::getJoule(double I, double t)const{
    cout<<"Inserire l'intensità di corrente in ampere"<<endl;
    if(isnan(I))
        cin>>I;
    if(I<0) throw num_neg();
    cout<<"Inserire l'intervallo di tempo in secondi"<<endl;
    if(isnan(t))
        cin>>t;
    if(t<0) throw num_neg();
    return getValore()*I*I*t;
}



unsigned short int resistenza::getValBanda(const Colore& c)const{
    return valore[c];
}
float resistenza::getValToll(const Colore& c)const{
    return tolleranza[c];
}
double resistenza::getValMolt(const Colore& c)const{
    return coloremoltiplicatore[c];
}

Colore resistenza::getCol(char c)const{
    return colori_bande[c];
}



istream& operator>>(istream& is, resistenza* & r){
    int l=0;
    cout<<"Inserisci il numero di bande che vuoi inserire 4/5/6"<<endl;
    cin>>l;
    if(l<4 || l>6)
        throw err_nbande();
    if(l==4){
        Res4bande r4;
        try{cin>>r4;}
        catch (err_sint){cout<<"errore sintattico"<<endl;  r4=Res4bande('N','N','N','a');}
        catch (fine_file){cout<<"errore EOF"<<endl; r4=Res4bande('N','N','N','a');}
        catch (err_banda1){cout<<"errore nella prima banda"<<endl; r4=Res4bande('N','N','N','a');}
        catch (err_banda2){cout<<"errore nella seconda banda"<<endl; r4=Res4bande('N','N','N','a');}
        catch (err_banda3){cout<<"errore nella terza banda"<<endl; r4=Res4bande('N','N','N','a');}
        catch (err_banda4){cout<<"errore nella quarta banda"<<endl;  r4=Res4bande('N','N','N','a');}
        r=new Res4bande(r4);
    }
    else if(l==5){
        Res5bande r5;
        try{cin>>r5;}
        catch (err_sint){cout<<"errore sintattico"<<endl; r5=Res5bande('N','N','N','a','a');}
        catch (fine_file){cout<<"errore EOF"<<endl; r5=Res5bande('N','N','N','a','a');}
        catch (err_banda1){cout<<"errore nella prima banda"<<endl; r5=Res5bande('N','N','N','a','a');}
        catch (err_banda2){cout<<"errore nella seconda banda"<<endl;r5=Res5bande('N','N','N','a','a');}
        catch (err_banda3){cout<<"errore nella terza banda"<<endl; r5=Res5bande('N','N','N','a','a');}
        catch (err_banda4){cout<<"errore nella quarta banda"<<endl; r5=Res5bande('N','N','N','a','a');}
        catch (err_banda5){cout<<"errore nella quinta banda"<<endl; r5=Res5bande('N','N','N','a','a');}
        r=new Res5bande(r5);
    }
    else if(l==6){
        Res6bande r6;
        try{cin>>r6;}
        catch (err_sint){cout<<"errore sintattico"<<endl; r6=Res6bande('N','N','N','a','a','N');}
        catch (fine_file){cout<<"errore EOF"<<endl; r6=Res6bande('N','N','N','a','a','N');}
        catch (err_banda1){cout<<"errore nella prima banda"<<endl; r6=Res6bande('N','N','N','a','a','N');}
        catch (err_banda2){cout<<"errore nella seconda banda"<<endl; r6=Res6bande('N','N','N','a','a','N');}
        catch (err_banda3){cout<<"errore nella terza banda"<<endl; r6=Res6bande('N','N','N','a','a','N');}
        catch (err_banda4){cout<<"errore nella quarta banda"<<endl; r6=Res6bande('N','N','N','a','a','N');}
        catch (err_banda5){cout<<"errore nella quinta banda"<<endl; r6=Res6bande('N','N','N','a','a','N');}
        catch (err_banda6){cout<<"errore nella sesta banda"<<endl; r6=Res6bande('N','N','N','a','a','N'); }
        r=new Res6bande(r6);
    }
    return is;
}

