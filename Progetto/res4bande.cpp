#include "res4bande.h"

#include "eccezioni.h"

Res4bande::Res4bande(char f1, char f2, char f3, char f4){
    v.clear();
    char c=f1;
    string::size_type pos;
    string valori_validi("NMRAYVBPGW");
    int cont=1;
        for(int i=0; i<3; i++){
            switch(i){
            case 0:
                if(cont==0) c=f2;
                pos = valori_validi.find(c);
                if(pos==string::npos && cont==1 ) throw err_banda1();
                if(pos==string::npos && cont==0) throw err_banda2();
                if(cont>0){
                    i--;
                    cont--;
                }
                break;
            case 1:
                c=f3;
                valori_validi = "NMRAYVBPoa";
                pos = valori_validi.find(c);
                if(pos==string::npos) throw err_banda3();
                break;
            case 2:
                c=f4;
                valori_validi = "oa";
                pos = valori_validi.find(c);
                if(pos==string::npos) throw err_banda4();
                break;
        }
        v.push_back(getCol(c));
    }
}

double Res4bande::getValore()const{
    return (getValBanda(v[0])*10+getValBanda(v[1]))*getValMolt(v[2]);
}

void Res4bande::getallVal() const{
    cout<<getValore()<<" ohm +-"<<getValToll(v[3])<<"%"<<endl;
}

Res4bande* Res4bande::clone() const{
    return new Res4bande(*this);
}

ostream& operator<<(ostream& os, const Res4bande& r){
    for(unsigned int i=0; i<r.v.size(); ++i) os<<r.v[i];
    return os;
}

istream& operator>>(istream& is, Res4bande& r){
    (r.v).clear();
    cout<<"   | N = nero | M = marrone | R = rosso | A = arancione | Y = giallo | V = verde | B = blu | P = viola | G = grigio |   "<<"   | W = bianco | o = oro | a = argento | esempio: [Y,P,A,o]"<<endl;
    char c;
    string::size_type pos;
    string valori_validi("NMRAYVBPGW");
    if(!(is>>c)) throw fine_file();
    if(c!='[') throw err_sint() ;
    //abbiamo letto la prima parentesi, dobbiamo leggere almeno 3 colori seguiti da ,
    int cont=1;
        for(int i=0; i<3; i++){
            if(!(is>>c)) throw fine_file();
            switch(i){
            case 0:
                pos = valori_validi.find(c);
                if(pos==string::npos && cont==1 ) throw err_banda1();
                if(pos==string::npos && cont==0) throw err_banda2();
                if(cont>0){
                    i--;
                    cont--;
                }
                break;
            case 1:
                valori_validi = "NMRAYVBPoa";
                pos = valori_validi.find(c);
                if(pos==string::npos) throw err_banda3();
                break;
            case 2:
                valori_validi = "oa";
                pos = valori_validi.find(c);
                if(pos==string::npos) throw err_banda4();
                break;
            }
            (r.v).push_back(resistenza::colori_bande[c]);
            if(i<2){
               if(!(is>>c)) throw fine_file();
                if(c!=',') throw err_sint() ;
            }
        }
 if(!(is>>c)) throw fine_file();
 if(c!=']') throw err_sint();
 return is;
}
