#ifndef LISTACOLORI_H
#define LISTACOLORI_H
#define C_NERO      {0,0,0}
#define C_MARRONE   {150,75,0}
#define C_ROSSO     {255,0,0}
#define C_ARANCIONE {255,165,0}
#define C_GIALLO    {255,255,0}
#define C_VERDE     {0,255,0}
#define C_BLU       {0,0,255}
#define C_VIOLA     {143,0,255}
#define C_GRIGIO    {128,128,128}
#define C_BIANCO    {255,255,255}
#define C_ORO       {255,215,0}
#define C_ARGENTO   {192,192,192}
#endif // LISTACOLORI_H
