QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app
CONFIG += console c++11

SOURCES += main.cpp \
    mainwindow.cpp \
    colore.cpp \
    resistenza.cpp \
    res4bande.cpp \
    res5bande.cpp \
    res6bande.cpp \
    stardelta.cpp

HEADERS += \
    mainwindow.h \
    colore.h \
    eccezioni.h \
    resistenza.h \
    listacolori.h \
    res4bande.h \
    res5bande.h \
    res6bande.h \
    stardelta.h

FORMS += \
        mainwindow.ui

RESOURCES += \
    img.qrc
