#ifndef RES6BANDE_H
#define RES6BANDE_H
#include "resistenza.h"
#include <math.h>

class Res6bande : public resistenza
{
    friend istream& operator>>(istream&, Res6bande&);
    friend ostream& operator<<(ostream&, const Res6bande&);
private:
    vector<Colore> v;
    static map<Colore,short int> coeff_temperatura;

public:
    Res6bande(char='N', char='N', char='N', char='a', char='a',char='N');
    virtual double getValore() const;
    virtual void getallVal() const;
    virtual Res6bande* clone() const;
    double ohmTemp(double=NAN, double=NAN)const;
};
ostream& operator<<(ostream&, const Res6bande&);

istream& operator>>(istream&, Res6bande&);
#endif // RES6BANDE_H
