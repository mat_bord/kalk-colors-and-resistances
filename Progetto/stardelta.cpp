#include "stardelta.h"

StarDelta::StarDelta(){
    for(int i=0; i<3;i++)
    starDeltaInput[i] = nullptr;
}

void StarDelta::deltaToStar(){
    if(starDeltaInput[0]!=nullptr && starDeltaInput[1]!=nullptr && starDeltaInput[2]!=nullptr){
        double valore0 = starDeltaInput[0]->getValore();
        double valore1 = starDeltaInput[1]->getValore();
        double valore2 = starDeltaInput[2]->getValore();
        if((valore0+valore1+valore2)==0)
            throw err_divisione();
        starDeltaOutput[0]=((valore0*valore2)/(valore0+valore1+valore2));
        starDeltaOutput[1]=((valore0*valore1)/(valore0+valore1+valore2));
        starDeltaOutput[2]=((valore1*valore2)/(valore0+valore1+valore2));
        for(int i=0; i<3; ++i){
            delete starDeltaInput[i];
            starDeltaInput[i]=nullptr;
        }
    }
}

void StarDelta::starToDelta(){
    if(starDeltaInput[0]!=nullptr && starDeltaInput[1]!=nullptr && starDeltaInput[2]!=nullptr){
        double valore0 = starDeltaInput[0]->getValore();
        double valore1 = starDeltaInput[1]->getValore();
        double valore2 = starDeltaInput[2]->getValore();
        if(valore0==0 || valore1==0 || valore2==0)
            throw err_divisione();
        starDeltaOutput[0]=((valore0*valore1+valore0*valore2+valore1*valore2)/(valore0));
        starDeltaOutput[1]=((valore0*valore1+valore0*valore2+valore1*valore2)/(valore1));
        starDeltaOutput[2]=((valore0*valore1+valore0*valore2+valore1*valore2)/(valore2));
        for(int i=0; i<3; ++i){
            delete starDeltaInput[i];
            starDeltaInput[i]=nullptr;
        }
    }
}

void StarDelta::InsertDelta1( resistenza* & r){
    if(starDeltaInput[0]!= nullptr)
        delete starDeltaInput[0];
    starDeltaInput[0]=r->clone();
}

void StarDelta::InsertDelta2( resistenza* & r){
    if(starDeltaInput[1]!= nullptr)
        delete starDeltaInput[1];
    starDeltaInput[1]=r->clone();
}

void StarDelta::InsertDelta3( resistenza* & r){
    if(starDeltaInput[2]!= nullptr)
        delete starDeltaInput[2];
    starDeltaInput[2]=r->clone();
}

double StarDelta::getDelta(int i)const{
    return starDeltaOutput[i];
}
StarDelta::~StarDelta(){
    for(int i=0;i<3;i++)
        delete starDeltaInput[i];
}
