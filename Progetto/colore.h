#ifndef COLORE_H
#define COLORE_H
#include <string>
using namespace std;

class Colore
{
friend ostream& operator<<(ostream&, const Colore&);
friend istream& operator>>(istream&, Colore&);
private:
    unsigned short int Rosso;
    unsigned short int Verde;
    unsigned short int Blu;
    static Colore Colore_Bianco;
    static char hex[16];
public:
    Colore(unsigned short int R=0, unsigned short int G=0, unsigned short int B=0)
        :Rosso(R), Verde(G), Blu(B){}
    string getHex() const;
    void coloreComplementare();
    Colore mixDueColori(const Colore&) const;
    void convertiAScalaGrigi();
    Colore operator%(int);
    int luminanza() const;
    Colore operator+(const Colore&)const;
    Colore operator-(const Colore&)const;
    unsigned short int getRosso()const;
    unsigned short int getVerde()const;
    unsigned short int getBlu()const;
    char confronto(const Colore&)const;
    bool operator<(const Colore&)const;
    Colore& operator=(const Colore&);

};
ostream& operator<<(ostream&, const Colore&);
istream& operator>>(istream&, Colore&);
#endif // COLORE_H
