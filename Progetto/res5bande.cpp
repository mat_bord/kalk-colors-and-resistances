#include "res5bande.h"
#include "eccezioni.h"

Res5bande::Res5bande(char f1, char f2, char f3, char f4, char f5){
    v.clear();
    char c=f1;
    string::size_type pos;
    string valori_validi("NMRAYVBPGW");
    int cont=2;
    for(int i=0; i<3; i++){
            switch(i){
            case 0:
                if(cont==1) c=f2;
                if(cont==0) c=f3;
                pos = valori_validi.find(c);
                if(pos==string::npos && cont==2 ) throw err_banda1();
                if(pos==string::npos && cont==1) throw err_banda2();
                if(pos==string::npos && cont==0) throw err_banda3();
                if(cont>0){
                    i--;
                    cont--;
                }
                break;
            case 1:
                c=f4;
                valori_validi = "NMRAYVBPoa";
                pos = valori_validi.find(c);
                if(pos==string::npos) throw err_banda4();
                break;
            case 2:
                c=f5;
                valori_validi = "GPBVRMoa";
                pos = valori_validi.find(c);
                if(pos==string::npos) throw err_banda5();
                break;
            }
            v.push_back(getCol(c));
        }
}


double Res5bande::getValore()const{
    return (getValBanda(v[0])*100+getValBanda(v[1])*10+getValBanda(v[2]))*getValMolt(v[3]);
}

void Res5bande::getallVal() const{
    cout<<getValore()<<" ohm +-"<<getValToll(v[4])<<"%"<<endl;
}

Res5bande* Res5bande::clone() const{
    return new Res5bande(*this);
}

ostream& operator<<(ostream& os, const Res5bande& r){
    for(unsigned int i=0; i<r.v.size(); ++i) os<<r.v[i];
    return os;
}

istream& operator>>(istream& is, Res5bande& r){
    (r.v).clear();
    cout<<"   | N = nero | M = marrone | R = rosso | A = arancione | Y = giallo | V = verde | B = blu | P = viola | G = grigio |   "<<"   | W = bianco | o = oro | a = argento | esempio: [Y,P,A,o,a]"<<endl;
    char c;
    string::size_type pos;
    string valori_validi("NMRAYVBPGW");
    if(!(is>>c)) throw fine_file();
    if(c!='[') throw err_sint() ;
    //abbiamo letto la prima parentesi, dobbiamo leggere almeno 3 colori seguiti da ,
    int cont=2;
        for(int i=0; i<3; i++){
            if(!(is>>c)) throw fine_file();
            switch(i){
            case 0:
                pos = valori_validi.find(c);
                if(pos==string::npos && cont==2 ) throw err_banda1();
                if(pos==string::npos && cont==1) throw err_banda2();
                if(pos==string::npos && cont==0) throw err_banda3();
                if(cont>0){
                    i--;
                    cont--;
                }
                break;
            case 1:
                valori_validi = "NMRAYVBPoa";
                pos = valori_validi.find(c);
                if(pos==string::npos) throw err_banda4();
                break;
            case 2:
                valori_validi = "GPBVRMoa";
                pos = valori_validi.find(c);
                if(pos==string::npos) throw err_banda5();
                break;
            }
            (r.v).push_back(resistenza::colori_bande[c]);
            if(i<2){
               if(!(is>>c)) throw fine_file();
                if(c!=',') throw err_sint() ;
            }
        }
 if(!(is>>c)) throw fine_file();
 if(c!=']') throw err_sint();
 return is;
}

