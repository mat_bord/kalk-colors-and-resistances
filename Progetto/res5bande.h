#ifndef RES5BANDE_H
#define RES5BANDE_H
#include "resistenza.h"
class Res5bande : public resistenza
{
    friend istream& operator>>(istream&, Res5bande&);
    friend ostream& operator<<(ostream&, const Res5bande&);
private:
    vector<Colore> v;
public:
    Res5bande(char='N', char='N', char='N', char='a', char='a');
    virtual double getValore() const;
    virtual void getallVal() const;
    virtual Res5bande* clone() const;
};
ostream& operator<<(ostream&, const Res5bande&);

istream& operator>>(istream&, Res5bande&);
#endif // RES5BANDE_H
