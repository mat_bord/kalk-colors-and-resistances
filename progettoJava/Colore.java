package progettoJava;


public class Colore {
    private int  Rosso=0;
    private int  Verde=0;
    private int  Blu=0;
    private static Colore Colore_bianco=new Colore(255,255,255);
    private static char[] hex={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
    public Colore(int R, int G, int B){
        Rosso=R;
        Verde=G;
        Blu=B;
    }
    public Colore() {}
    public char[] getHex(){
        int col=Rosso ,i=0;
        char[] esa= {'0','0','0','0','0','0'} ;
        while(i!=6){
            esa[i]=hex[col/16];
            i++;
            if(i==2)
                col=Verde;
            if(i==4)
                col=Blu;
        }
        return esa;
    }
    
    public Colore minus(Colore c) {
    	Colore differenza=new Colore();
    	if(c.Rosso > Rosso)differenza.Rosso = 0;
        else differenza.Rosso = Rosso-c.Rosso;
        if(c.Verde > Verde)differenza.Verde = 0;
        else differenza.Verde = Verde-c.Verde;
        if(c.Blu > Blu)differenza.Blu = 0;
        else differenza.Blu = Blu-c.Blu;
        return differenza;
    }
    
    public Colore plus( Colore c){
        Colore somma=new Colore();
        somma.Rosso = Rosso+c.Rosso;
        if(somma.Rosso > 255) somma.Rosso = 255;
        somma.Verde = Verde+c.Verde;
        if(somma.Verde > 255) somma.Verde = 255;
        somma.Blu = Blu+c.Blu;
        if(somma.Blu > 255) somma.Blu = 255;
        return somma;
    }
    
    public void assegnazione(Colore c) {
    	Rosso=c.Rosso;
    	Verde=c.Verde;
    	Blu=c.Blu;
    }
    
    public void coloreComplementare(){
        this.assegnazione((Colore_bianco.minus(this)));        
    }

    public Colore mixDueColori(Colore c){
        Colore mix=new Colore();
        mix.Rosso=(Rosso+c.Rosso)/2;
        mix.Verde=(Verde+c.Verde)/2;
        mix.Blu=(Blu+c.Blu)/2;
        return mix;
    }

    public void convertiAScalaGrigi(){
        Blu=Verde=Rosso=(Rosso+Verde+Blu)/3;
    }
    
    public Colore percentuale(int n){
        Colore percentuale=new Colore();
        percentuale.Rosso=Rosso-Math.round((float)((Rosso)*n/100));
        percentuale.Verde=Verde-Math.round((float)((Verde)*n/100));
        percentuale.Blu=Blu-Math.round((float)((Blu)*n/100));
        return percentuale;
    }
    
    public long luminanza(){
        double l=0.2126*Rosso+0.7152*Verde+0.0722*Blu;
        return Math.round(l);
    }
    
    public char confronto(Colore c){
        if((Rosso+Verde+Blu)==(c.Rosso+c.Verde+c.Blu))return '=';
        if((Rosso+Verde+Blu)<(c.Rosso+c.Verde+c.Blu))return '<';
        else return '>';
    }

    public boolean minore( Colore c){
        if((Rosso+Verde+Blu)<(c.Rosso+c.Verde+c.Blu))return true;
        else return false;
    }   
    
    public int getRosso(){
        return Rosso;
    }
    
    public int getVerde(){
        return Verde;
    }
    
    public int getBlu(){
        return Blu;
    }
    
    public void stampa() {
    	System.out.println("("+Rosso+","+Verde+","+Blu+")");
    }
}
