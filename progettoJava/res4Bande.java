package progettoJava;



public class res4Bande extends resistenza{
private Colore[] va= new Colore[4];

public res4Bande(char f1, char f2, char f3, char f4) throws eccezioni{
    boolean co=false;
    int cont=1;
        for(int i=0; i<3; i++){
            switch(i){
            case 0:
            	co=false;
            	for(int  v=0; v<10 && co==false;v++) {
                	char[] valori_validi= {'N','M','R','A','Y','V','B','P','G','W'};
            		if(cont==1 && valori_validi[v]==f1) {
            			va[0]=getCol(f1);
            			co=true;
            		}
            		if(cont==0 && valori_validi[v]==f2) {
            			va[1]=getCol(f2);
            			co=true;
            		}
            	}
                if(co==false && cont==1 ) throw new eccezioni("err_banda1");
                if(co==false && cont==0) throw new eccezioni("err_banda2");
                if(cont>0){
                    i--;
                    cont=cont-1;
                }
                break;
            case 1:
            	co=false;
            	for(int  v=0; v<12 && co==false;v++) {
                	char[] valori_validi= {'N','M','R','A','Y','V','B','P','G','W','o','a'};
                	if(valori_validi[v]==f3) {	
            			va[2]=getCol(f3);
            			co=true;
            		}
            	}
                if(co==false) throw new eccezioni("err_banda3");
                break;
            case 2:
            	co=false;
            	for(int  v=0; v<2 && co==false;v++) {
                	char[] valori_validi= {'o','a'};
            		if(valori_validi[v]==f4) {
            			va[3]=getCol(f4);
            			co=true;
            		}
            	}
                if(co==false) throw new eccezioni("err_banda4");
                break;
        }
            
    }
}

public double getValore(){
    return (getValBanda(va[0])*10+getValBanda(va[1]))*getValMolt(va[2]);
}

public void getallVal() {
    System.out.println(getValore()+" ohm +-"+getValToll(va[3])+"%");
}

public void stampa(){
    for(int i=0; i<4; ++i) va[i].stampa();

}

		
}
