package progettoJava;


public class Use {
	public static void main(String [ ] args) throws eccezioni
	{
		
		Colore uno=new Colore(19,230,210);
		Colore due=new Colore(0,200,174);
		System.out.println("Valore primo colore");
		uno.stampa();
		System.out.println("Valore secondo colore");
		due.stampa();
		uno=uno.minus(due);
		System.out.println("Primo meno secondo");
		uno.stampa();
		Colore tre=new Colore(0,0,0);
		System.out.println("Assegnazione del secondo ad un terzo");
		tre.assegnazione(due);
		tre.stampa();
		due=uno.plus(due);
		System.out.println("Primo piu secondo");
		due.stampa();
		System.out.println("Colore complementare");
		tre.coloreComplementare();
		tre.stampa();
		System.out.println("Scala di grigi");
		tre.convertiAScalaGrigi();
		tre.stampa();
		System.out.println("Mix del primo e del terzo");
		uno.mixDueColori(tre);
		uno.stampa();
		System.out.println("50%");
		due.percentuale(50);
		due.stampa();
		System.out.println("Luminanza");
		System.out.println(due.luminanza()+" Nit");
		
		System.out.println("RESISTENZE");
		resistenza r4=new res4Bande('M','M','N','o');
		resistenza r5=new res5Bande('R','A','Y','M','a');
		resistenza r6=new res6Bande('R','A','Y','M','a','P');
		System.out.println("Valore r4");
		r4.getallVal();
		System.out.println("Valore r5");
		r5.getallVal();
		System.out.println("Valore r6");
		r6.getallVal();
		System.out.println("r4<r5?");
		System.out.println(r4.minore(r5));
		System.out.println("r4>r5?");
		System.out.println(r4.maggiore(r5));
		System.out.println("r5==r6?");
		System.out.println(r5.uguaglianza(r6));
		System.out.println("r4 e r5 in serie");
		System.out.println(resistenza.serie(r4,r5)+" ohm");
		System.out.println("r4 e r5 in parallelo");
		System.out.println(resistenza.parallelo(r4,r6)+" ohm");
		System.out.println("Valore in ohm di r5");
		System.out.println(r5.leggeohm()+"A");
		System.out.println("Rumore di r6");
		System.out.println(r6.getRumore()+"volt");
		System.out.println("Joule di r4");
		System.out.println(r4.getJoule()+"J");
		StarDelta sd;
		sd.InsertDelta1(r4);
		sd.InsertDelta2(r5);
		sd.InsertDelta3(r6);
		System.out.println("Delta->Star di r4, r5 e r6");
		sd.deltaToStar();
		for(int i=0;i<3;i++)
			System.out.println(resistenza.getDelta(i)+"ohm");
		if(r6 instanceof res6Bande) {
			System.out.println("Resistivita al lavoro di r6");
			System.out.println(r6.ohmTemp()+" ohm");
		}
		else {
			System.out.println("La resistenza non � a 6 bande");
		}
	}
}

