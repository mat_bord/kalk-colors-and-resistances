package progettoJava;
import java.util.*;

public abstract class StarDelta {    	

    
	private resistenza[] starDeltaInput=new resistenza[3];
	private double[] starDeltaOutput= {0.0,0.0,0.0};
    

    public void deltaToStar(){
    if(starDeltaInput[0]!=null && starDeltaInput[1]!=null && starDeltaInput[2]!=null){
        double valore0 = starDeltaInput[0].getValore();
        double valore1 = starDeltaInput[1].getValore();
        double valore2 = starDeltaInput[2].getValore();
        starDeltaOutput[0]=((valore0*valore2)/(valore0+valore1+valore2));
        starDeltaOutput[1]=((valore0*valore1)/(valore0+valore1+valore2));
        starDeltaOutput[2]=((valore1*valore2)/(valore0+valore1+valore2));
        for(int i=0; i<3; ++i){
            starDeltaInput[i]=null;
        }
    }
}

    public void starToDelta(){
    if(starDeltaInput[0]!=null && starDeltaInput[1]!=null && starDeltaInput[2]!=null){

        double valore0 = starDeltaInput[0].getValore();
        double valore1 = starDeltaInput[1].getValore();
        double valore2 = starDeltaInput[2].getValore();
        starDeltaOutput[0]=((valore0*valore1+valore0*valore2+valore1*valore2)/(valore0));
        starDeltaOutput[1]=((valore0*valore1+valore0*valore2+valore1*valore2)/(valore1));
        starDeltaOutput[2]=((valore0*valore1+valore0*valore2+valore1*valore2)/(valore2));
        for(int i=0; i<3; ++i){
            starDeltaInput[i]=null;
        }
    }
}

    public void InsertDelta1(resistenza r) {
		starDeltaInput[0]=r;
}
    public void InsertDelta2(resistenza r) {
    starDeltaInput[1]=r;
}
    public void InsertDelta3(resistenza r){
    starDeltaInput[2]=r;
    
}
    public double getDelta(int i){
    return starDeltaOutput[i];
}

}

