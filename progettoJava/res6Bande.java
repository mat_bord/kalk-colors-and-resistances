package progettoJava;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


public class res6Bande extends resistenza {
	private Colore[] va= new Colore[6];
	private static final Map<Colore, Integer> coeff_temperatura=new HashMap<Colore, Integer>();
	static {
		coeff_temperatura.put(C_NERO, 200);
		coeff_temperatura.put(C_MARRONE, 100);
		coeff_temperatura.put(C_ROSSO, 50);
		coeff_temperatura.put(C_ARANCIONE, 15);
		coeff_temperatura.put(C_GIALLO, 25);
		coeff_temperatura.put(C_BLU, 10);
		coeff_temperatura.put(C_VIOLA, 5);

	}
	
	public res6Bande(char f1, char f2, char f3, char f4, char f5, char f6) throws eccezioni{
	    boolean co=false;
	    int cont=2;
	        for(int i=0; i<4; i++){
	            switch(i){
	            case 0:
	            	co=false;
	            	for(int  v=0; v<10 && co==false;v++) {
	                	char[] valori_validi= {'N','M','R','A','Y','V','B','P','G','W'};

	            		if(cont==2 && valori_validi[v]==f1) {
	            			va[0]=getCol(f1);
	            			co=true;
	            		}
	            		if(cont==1 && valori_validi[v]==f2) {
	            			va[1]=getCol(f2);
	            			co=true;
	            		}
	            		if(cont==0 && valori_validi[v]==f3) {
	            			va[2]=getCol(f3);
	            			co=true;
	            		}
	            	}
	            	 if(co==false && cont==2) throw new eccezioni("err_banda1");
	                if(co==false && cont==1 ) throw new eccezioni("err_banda2");
	                if(co==false && cont==0) throw new eccezioni("err_banda3");
	                if(cont>0){
	                    i--;
	                    cont--;
	                }
	                break;
	            case 1:
	            	co=false;
	            	for(int  v=0; v<12 && co==false;v++) {
	                	char[] valori_validi= {'N','M','R','A','Y','V','B','P','G','W','o','a'};
	            		if(valori_validi[v]==f4) {
	            			va[3]=getCol(f4);
	            			co=true;
	            		}
	            	}
	                if(co==false) throw new eccezioni("err_banda4");
	                break;
	            case 2:
	            	co=false;
	            	for(int  v=0; v<2 && co==false;v++) {
	                	char[] valori_validi= {'o','a'};
	            		if(valori_validi[v]==f5) {
	            			va[4]=getCol(f5);
	            			co=true;
	            		}
	            	}
	                if(co==false) throw new eccezioni("err_banda5");
	                break;
	            case 3:
	            	co=false;
	            	for(int  v=0; v<7 && co==false;v++) {
	            		char[] valori_validi= {'N','M','R','A','Y','B','P'}; 
	            		if(valori_validi[v]==f6) {
	            			va[5]=getCol(f6);
	            			co=true;
	            		}
	            	}
	                if(co==false) throw new eccezioni("err_banda6");
	                break;
	            }
	        }
	    }
	
	public double getValore(){
	    return (getValBanda(va[0])*10+getValBanda(va[1])*10+getValBanda(va[2]))*getValMolt(va[3]);
	}

	public void getallVal(){
		System.out.println(getValore()+" ohm +- "+getValToll(va[4])+"% "+coeff_temperatura.get(va[5])+"ppm/k");
	}
	
	public void stampa(){
	    for(int i=0; i<6; ++i) va[i].stampa();

	}
	
	public double ohmTemp(){
		double T0=22;
		double T=40;
	    //System.out.println("Inserisci la temperatura attuale");
	    //Scanner input=new Scanner(System.in);
		//T0=input.nextDouble();
	    //System.out.println("Inserisci la temmperatura finale");
	    //input=new Scanner(System.in);
		//T=input.nextDouble();
	    return getValore()*(1+coeff_temperatura.get(va[5])*(T-T0));
	}
}
