package progettoJava;
import java.util.*;

public abstract class resistenza {    	
		protected static Colore C_NERO=new Colore(0,0,0);
		protected static Colore C_MARRONE=new Colore(150,75,0);
		protected static Colore C_ROSSO=new Colore(255,0,0);
		protected static Colore C_ARANCIONE=new Colore(255,165,0);
		protected static Colore C_GIALLO=new Colore(255,255,0);
		protected static Colore C_VERDE=new Colore(0,255,0);
		protected static Colore C_BLU=new Colore(0,0,255);
		protected static Colore C_VIOLA=new Colore(143,0,255);
		protected static Colore C_GRIGIO=new Colore(128,128,128);
		protected static Colore C_BIANCO=new Colore(255,255,255);
		protected static Colore C_ORO=new Colore(255,215,0);
		protected static Colore C_ARGENTO=new Colore(192,192,192);
	private static final Map<Character, Colore> colori_bande=new HashMap<Character, Colore>();
	private static final Map<Colore,Integer> valore=new HashMap<Colore, Integer>();
	private static final Map<Colore,Double> tolleranza=new HashMap<Colore, Double>();
	private static final Map<Colore,Double> coloremoltiplicatore=new HashMap<Colore, Double>();
	static {  
    	colori_bande.put('N', C_NERO);
    	colori_bande.put('M', C_MARRONE);
    	colori_bande.put('R', C_ROSSO);
    	colori_bande.put('A', C_ARANCIONE);
    	colori_bande.put('Y', C_GIALLO);
    	colori_bande.put('V', C_VERDE);
    	colori_bande.put('B', C_BLU);
    	colori_bande.put('P', C_VIOLA);
    	colori_bande.put('G', C_GRIGIO);
    	colori_bande.put('W', C_BIANCO);
    	colori_bande.put('o', C_ORO);
    	colori_bande.put('a', C_ARGENTO);
    	valore.put(C_NERO, 0);
    	valore.put(C_MARRONE, 1);
    	valore.put(C_ROSSO, 2);
    	valore.put(C_ARANCIONE, 3);
    	valore.put(C_GIALLO, 4);
    	valore.put(C_VERDE, 5);
    	valore.put(C_BLU, 6);
    	valore.put(C_VIOLA, 7);
    	valore.put(C_GRIGIO, 8);
    	valore.put(C_BIANCO, 9);
    	tolleranza.put(C_GRIGIO, 0.05);
    	tolleranza.put(C_VIOLA, 0.1);
    	tolleranza.put(C_BLU, 0.25);
    	tolleranza.put(C_VERDE, 0.5);
    	tolleranza.put(C_ROSSO, 2.0);
    	tolleranza.put(C_MARRONE,1.0);
    	tolleranza.put(C_ORO, 5.0);
    	tolleranza.put(C_ARGENTO, 10.0);
    	coloremoltiplicatore.put(C_NERO, 1.0);
    	coloremoltiplicatore.put(C_MARRONE, 10.0);
    	coloremoltiplicatore.put(C_ROSSO, 100.0);
    	coloremoltiplicatore.put(C_ARANCIONE, 1000.0);
    	coloremoltiplicatore.put(C_GIALLO, 10000.0);
    	coloremoltiplicatore.put(C_VERDE, 10000.0);
    	coloremoltiplicatore.put(C_BLU, 1000000.0);
    	coloremoltiplicatore.put(C_VIOLA, 10000000.0);
    	coloremoltiplicatore.put(C_ORO, 0.1);
    	coloremoltiplicatore.put(C_ARGENTO, 0.01);
    }
    
    private static double constBoltzman=1.38e-23;
    public resistenza(){}
    public abstract double getValore();
    
    public abstract void  getallVal();
    
    public boolean minore( resistenza r){
        return (getValore()<r.getValore());
    }
    
    public boolean maggiore( resistenza r){
        return getValore()>r.getValore();
    }
    
    public boolean uguaglianza( resistenza r){
        return getValore()==r.getValore();
    }
    
    static public double serie( resistenza r1, resistenza r2){
        return r1.getValore() + r2.getValore();
    }
    
    static public double parallelo( resistenza r1, resistenza r2){
        return (1/r1.getValore()) + (1/r2.getValore());
    }
    
    public double leggeohm(){
    	double v=5;
        //System.out.println("Inserisci la differenza di potenziale elettrico V");
    	//Scanner input=new Scanner(System.in);
    	//v=input.nextDouble();
        return v/getValore();
        
        
    }
    
    public double getRumore() throws eccezioni{
    	double k=320;
    	double f=30;
    	//System.out.println("Inserire la temperatura in kelvin");
    	//Scanner input=new Scanner(System.in);
    	//k=input.nextDouble();
    	throw new eccezioni("Sintassi");
    	//System.out.println("Inserire la media delle frequenze considerate in Hz");
    	//input=new Scanner(System.in);
    	//f=input.nextDouble();   	
        return Math.sqrt(4*constBoltzman*getValore()*k*f);
}

    public double getJoule(){
    double I=0.67;
    double t=5;
	//System.out.println("Inserire l'intensita di corrente in ampere");
    //Scanner input=new Scanner(System.in);
	//I=input.nextDouble();
	//System.out.println("Inserire l'intervallo di tempo in secondi");
    //input=new Scanner(System.in);
	//t=input.nextDouble();
    return getValore()*I*I*t;
}

    public int getValBanda(Colore c){
    return valore.get(c);
}
    
    public double getValToll(Colore c){
    return tolleranza.get(c);
}
    public double getValMolt( Colore c){
    return coloremoltiplicatore.get(c);
}

public Colore getCol(char c){
    return colori_bande.get(c);
}

}

